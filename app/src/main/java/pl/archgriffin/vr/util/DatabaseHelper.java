package pl.archgriffin.vr.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.bean.Photo;
import pl.archgriffin.vr.bean.Transition;
import pl.archgriffin.vr.bean.Classroom;

/**
 * Created by tomas on 13.12.2016.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final String DATABASE_NAME = "pollub_vr";
    private static final int DATABASE_VERSION = 14;
    private static final String LOG_TAG = "DatabaseHelper";
    private Context context;
    private Dao<Photo, Long> photoDao;
    private Dao<Transition, Long> transitionDao;
    private Dao<Classroom, Long> classroomDao;
    private Dao<Employee, Long> employeeDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Photo.class);
            TableUtils.createTable(connectionSource, Transition.class);
            TableUtils.createTable(connectionSource, Classroom.class);
            TableUtils.createTable(connectionSource, Employee.class);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Error while creating database.");
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        dropDatabase();
        onCreate(database, connectionSource);
    }

    public void dropDatabase() {
        context.deleteDatabase(DatabaseHelper.DATABASE_NAME);
    }


    public Dao<Photo, Long> getPhotoDao() throws SQLException {
        if (photoDao == null) {
            photoDao = getDao(Photo.class);
        }
        return photoDao;
    }

    public Dao<Transition, Long> getTransitionDao() throws SQLException {
        if (transitionDao == null) {
            transitionDao = getDao(Transition.class);
        }
        return transitionDao;
    }

    public Dao<Classroom, Long> getClassroomDao() throws SQLException {
        if (classroomDao == null) {
            classroomDao = getDao(Classroom.class);
        }
        return classroomDao;
    }

    public Dao<Employee, Long> getEmployeeDao() throws SQLException {
        if (employeeDao == null) {
            employeeDao = getDao(Employee.class);
        }
        return employeeDao;
    }

    @Override
    public void close() {
        photoDao = null;
        transitionDao = null;
        classroomDao = null;
        super.close();
    }

    public void dropAllTables() {
        try {
            TableUtils.dropTable(connectionSource, Photo.class, true);
            TableUtils.dropTable(connectionSource, Transition.class, true);
            TableUtils.dropTable(connectionSource, Classroom.class, true);
            TableUtils.dropTable(connectionSource, Employee.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

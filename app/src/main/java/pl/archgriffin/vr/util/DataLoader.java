package pl.archgriffin.vr.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.bean.Photo;
import pl.archgriffin.vr.bean.Transition;

/**
 * Created by tomas on 13.12.2016.
 */

public class DataLoader {

    private final static String TAG = "DataLoader";

    private DatabaseHelper helper;
    private Context context;

    public DataLoader(Context context) {
        this.context = context;
        this.helper = new DatabaseHelper(context);
    }

    //todo zamienic void na boolean? albo na int? (tzn ile umieszczono zdjec w bazie)
    public void addPhotosToDatabase(String photoFileName) throws MultiplePhotosForCoordinatesException {
        List<Photo> photos = getPhotosWithConstraints(photoFileName);
        Log.i(TAG, "Adding " + photos.size() + " records.");
        try {
            Dao<Photo, Long> photoDao = helper.getPhotoDao();
            photoDao.create(photos);
        } catch (SQLException e) {
            Log.e(TAG, "Error while adding data to database.");
            e.printStackTrace();
        }
    }

    public void addEmployeeToDatabase(String employeeFileName) {
        List<Employee> employees = getEmployees(employeeFileName);
        try {
            Dao<Employee, Long> employeeDao = helper.getEmployeeDao();
            employeeDao.create(employees);
        } catch (SQLException e) {
            Log.e(TAG, "addEmployeeToDatabase: ", e);
        }
    }

    private List<Employee> getEmployees(String fileName) {
        List<Employee> list = new ArrayList<>();
        AssetManager assetManager = context.getAssets();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open(fileName + ".txt")))) {
            String line;
            while ((line = in.readLine()) != null) {
                Employee e = createEmployeeFromLine(line);
                list.add(e);
            }
        } catch (IOException e) {
            Log.e(TAG, "Error while opening data for employee.", e);
        }
        return list;
    }


    private Employee createEmployeeFromLine(String line) {
        Employee employee = new Employee();
        String[] fields = line.split(";");
        for (String field : fields) {
            String[] data = field.split(":");
            if (data.length > 1) {
                switch (data[0]) {
                    case "title":
                        employee.setTitle(data[1]);
                        break;
                    case "first":
                        employee.setFirstName(data[1]);
                        break;
                    case "sur":
                        employee.setSurname(data[1]);
                        break;
                    case "timetable":
                        employee.setTimetablePath(data[1]);
                        break;
                    case "website":
                        employee.setWebsite(data[1] + ":" + data[2]);
                        break;
                    case "photo":
                        employee.setPhotoPath(data[1]);
                        break;
                }
            }
        }
        return employee;
    }

    private List<Photo> getPhotosWithConstraints(String photoFileName) throws MultiplePhotosForCoordinatesException {
        AssetManager assetManager = context.getAssets();
        List<Photo> photosList = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open(photoFileName + ".txt")))) {
            String line;
            while ((line = in.readLine()) != null) {
                Photo p = createPhotoFromLine(line);
                if (photosList.contains(p)) {
                    throw new MultiplePhotosForCoordinatesException("Multiple photos for coordinates x=" + p.getX() + " y=" + p.getY());
                }
                photosList.add(p);
            }
        } catch (IOException e) {
            Log.e(TAG, "Error while opening data for photos.");
            e.printStackTrace();
        }
        return photosList;
    }

    private Photo createPhotoFromLine(String line) {
        Photo photo = new Photo();
        String[] fields = line.split(";");
        for (String field : fields) {
            String[] data = field.split(":");
            switch (data[0]) {
                case "x":
                    photo.setX(Integer.parseInt(data[1]));
                    break;
                case "y":
                    photo.setY(Integer.parseInt(data[1]));
                    break;
                case "path":
                    photo.setPath(data[1]);
                    break;
                case "description":
                    photo.setDescription(data[1]);
                    break;
            }
        }
        return photo;
    }

    //todo troszeczke lepiej, ale dalej chujowa
    public void addTransitions(String transitionsFileName) throws PhotoNotFoundException {
        AssetManager assetManager = context.getAssets();
        List<Transition> transitions = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open(transitionsFileName + ".txt")))) {
            String line;
            while ((line = in.readLine()) != null) {
                String[] fields = line.split(";");
                Photo p1 = new Photo();
                Photo p2 = new Photo();
                float rotation = 0;
                for (String field : fields) {
                    String[] data = field.split(":");
                    switch (data[0]) {
                        case "x1":
                            p1.setX(Integer.parseInt(data[1]));
                            break;
                        case "y1":
                            p1.setY(Integer.parseInt(data[1]));
                            break;
                        case "x2":
                            p2.setX(Integer.parseInt(data[1]));
                            break;
                        case "y2":
                            p2.setY(Integer.parseInt(data[1]));
                            break;
                        case "rotation":
                            rotation = Float.parseFloat(data[1]);
                            break;
                    }
                }
                p1 = getPhotoByXandY(p1);
                p2 = getPhotoByXandY(p2);
                transitions.add(new Transition(p1, p2, rotation));
            }
            Dao<Transition, Long> transitionDao = helper.getTransitionDao();
            transitionDao.create(transitions);
        } catch (IOException e) {
            Log.e(TAG, "Error while opening data for transitions.");
            e.printStackTrace();
        } catch (SQLException e) {
            Log.e(TAG, "Error while adding transitions to database.");
            e.printStackTrace();
        }
    }


    public void addClassrooms(String classroomsFileName) {
        AssetManager assetManager = context.getAssets();
        List<Classroom> classrooms = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(assetManager.open(classroomsFileName + ".txt")))) {
            String line;
            while ((line = in.readLine()) != null) {
                String[] fields = line.split(";");
                Classroom classroom = new Classroom();
                for (String field : fields) {
                    String[] data = field.split("::");
                    if (data.length > 1) {
                        switch (data[0]) {
                            case "number":
                                classroom.setNumber(data[1]);
                                break;
                            case "timetable":
                                classroom.setTimetable(data[1]);
                                break;
                            case "info":
                                classroom.setInfo(data[1]);
                                break;
                            case "photoId":
                                classroom.setmPhotoId(Integer.parseInt(data[1]));
                                break;
                        }
                    }
                }
                classrooms.add(classroom);
            }
            Dao<Classroom, Long> classroomDao = helper.getClassroomDao();
            classroomDao.create(classrooms);
        } catch (IOException e) {
            Log.e(TAG, "Error while opening data for classrooms.");
            e.printStackTrace();
        } catch (SQLException e) {
            Log.e(TAG, "Error while adding transitions to database.");
            e.printStackTrace();
        }
    }


    private Photo getPhotoByXandY(Photo input) throws SQLException, PhotoNotFoundException {
        Dao<Photo, Long> photoDao = helper.getPhotoDao();
        Map<String, Object> inputValues = new HashMap<>();
        inputValues.put(Photo.FIELD_NAME_X, input.getX());
        inputValues.put(Photo.FIELD_NAME_Y, input.getY());
        List<Photo> photos = photoDao.queryForFieldValues(inputValues);
        if (photos.isEmpty()) {
            throw new PhotoNotFoundException("There is no photo for x=" + input.getX() + " and y=" + input.getY());
        } else {
            return photos.get(0);
        }
    }

    public boolean isDatabaseEmpty() {
        boolean isEmpty = true;
        try {
            Dao<Photo, Long> photoDao = helper.getPhotoDao();
            isEmpty = photoDao.queryForAll().isEmpty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isEmpty;
    }

    public class PhotoNotFoundException extends Exception {
        public PhotoNotFoundException(String message) {
            super(message);
        }
    }

    public class MultiplePhotosForCoordinatesException extends Exception {
        public MultiplePhotosForCoordinatesException(String message) {
            super(message);
        }
    }
}

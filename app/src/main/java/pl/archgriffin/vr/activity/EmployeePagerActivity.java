package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.datasource.EmployeeLab;
import pl.archgriffin.vr.fragment.EmployeeDetailFragment;

/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeePagerActivity extends AppCompatActivity {

    private static final String EXTRA_EMPLOYEE_ID = "pl.koobe24.vrgui.employee_id";

    private ViewPager mViewPager;
    private List<Employee> mEmployees;

    public static Intent newIntent(Context context, long employeeId) {
        Intent intent = new Intent(context, EmployeePagerActivity.class);
        intent.putExtra(EXTRA_EMPLOYEE_ID, employeeId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_pager);

        long employeeId = getIntent().getLongExtra(EXTRA_EMPLOYEE_ID, 1);
        mViewPager = (ViewPager) findViewById(R.id.activity_employee_view_pager);
        mEmployees = EmployeeLab.getInstance(this).getEmployees();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Employee employee = mEmployees.get(position);
                return EmployeeDetailFragment.newInstance(employee.getId());
            }

            @Override
            public int getCount() {
                return mEmployees.size();
            }
        });
        for (int i = 0; i < mEmployees.size(); i++) {
            if (mEmployees.get(i).getId() == employeeId) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

}

package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.datasource.ClassroomLab;
import pl.archgriffin.vr.fragment.ClassroomDetailFragment;
import pl.archgriffin.vr.bean.Classroom;


/**
 * Created by tomas on 12.03.2017.
 */

public class ClassroomPagerActivity extends AppCompatActivity {
    private static final String EXTRA_CLASSROOM_ID = "pl.koobe24.vrgui.classroom_id";

    private ViewPager mViewPager;
    private List<Classroom> mClassrooms;

    public static Intent newIntent(Context context, long classroomId) {
        Intent intent = new Intent(context, ClassroomPagerActivity.class);
        intent.putExtra(EXTRA_CLASSROOM_ID, classroomId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom_pager);

        long classroomId = getIntent().getLongExtra(EXTRA_CLASSROOM_ID, 0);
        mViewPager = (ViewPager) findViewById(R.id.activity_classroom_view_pager);
        mClassrooms = ClassroomLab.getInstance(this).getClassrooms();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Classroom classroom = mClassrooms.get(position);
                return ClassroomDetailFragment.newInstance(classroom.getId());
            }

            @Override
            public int getCount() {
                return mClassrooms.size();
            }
        });
        for (int i = 0; i < mClassrooms.size(); i++) {
            if (mClassrooms.get(i).getId() == classroomId) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}

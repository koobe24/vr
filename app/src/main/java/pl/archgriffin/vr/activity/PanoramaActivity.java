package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.fragment.PanoramaFragment;


/**
 * Created by tomas on 01.04.2017.
 */

public class PanoramaActivity extends SingleFragmentActivity {
    private static final String TAG = PanoramaActivity.class.getSimpleName();
    private static final String START_ID = "start_id";
    private static final String DESTINATION_ID = "destination_id";
    private long mStart;
    private long mDestination;

    private PanoramaFragment mCurrentFragment;

    @Override
    protected Fragment createFragment() {
        mCurrentFragment = PanoramaFragment.newInstance(mStart, mDestination);
        return mCurrentFragment;
    }

    public static Intent newIntent(Context context, Classroom start, Classroom destination) {
        Intent intent = new Intent(context, PanoramaActivity.class);
        long startId;
        long destinationId;
        if (start != null) {
            startId = start.getmPhotoId();
        } else {
            //początek
            startId = 1;
        }
        if (destination != null) {
            destinationId = destination.getmPhotoId();
        } else {
            destinationId = -1;
        }
        Log.i(TAG, "newIntent: " + startId + " " + destinationId);
        intent.putExtra(START_ID, startId);
        intent.putExtra(DESTINATION_ID, destinationId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        mStart = intent.getLongExtra(START_ID, 1);
        mDestination = intent.getLongExtra(DESTINATION_ID, -1);
        super.onCreate(savedInstanceState);
    }


    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent event) {
        float yaxis = event.getAxisValue(MotionEvent.AXIS_HAT_Y);
        if (yaxis == 1.0f) {
            mCurrentFragment.moveToPrevious();
        } else if (yaxis == -1.0f) {
            mCurrentFragment.makeMove();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, permissions[i] + " was not granted.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }
}

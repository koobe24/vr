package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pl.archgriffin.vr.fragment.EmployeeListFragment;


/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return EmployeeListFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, EmployeeListActivity.class);
        return intent;
    }
}

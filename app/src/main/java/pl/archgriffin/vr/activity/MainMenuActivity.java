package pl.archgriffin.vr.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pl.archgriffin.vr.fragment.MainMenuFragment;
import pl.archgriffin.vr.util.DataLoader;
import pl.archgriffin.vr.util.DatabaseHelper;

/**
 * Created by tomas on 01.04.2017.
 */

public class MainMenuActivity extends SingleFragmentActivity {
    private static final String TAG = MainMenuActivity.class.getSimpleName();

    @Override
    protected Fragment createFragment() {
        return MainMenuFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainMenuActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reloadSampleData();
        askForPermissionsIfNeeded();
    }

    private void reloadSampleData() {
        DatabaseHelper helper = new DatabaseHelper(this);
        helper.dropDatabase();
        DataLoader loader = new DataLoader(getApplicationContext());
        if (loader.isDatabaseEmpty()) {
            try {
                loader.addPhotosToDatabase("photos");
                loader.addTransitions("photos_transitions");
                loader.addClassrooms("classrooms");
                loader.addEmployeeToDatabase("teachers");
            } catch (DataLoader.PhotoNotFoundException e) {
                Log.e(TAG, "Photo not found for one of transitions.");
                e.printStackTrace();
            } catch (DataLoader.MultiplePhotosForCoordinatesException e) {
                Log.e(TAG, "Multiple photos for one of coordinates.");
                e.printStackTrace();
            }
        }
    }

    private void askForPermissionsIfNeeded() {
        String[] neededPermissions = neededPermissions();
        if (neededPermissions.length != 0) {
            ActivityCompat.requestPermissions(this, neededPermissions, 1);
        }
    }

    private String[] neededPermissions() {
        List<String> neededPermissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            neededPermissions.add(Manifest.permission.INTERNET);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            neededPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        String[] permissionsArray = new String[neededPermissions.size()];
        permissionsArray = neededPermissions.toArray(permissionsArray);
        return permissionsArray;
    }
}

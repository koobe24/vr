package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pl.archgriffin.vr.fragment.ClassroomListFragment;


/**
 * Created by tomas on 12.03.2017.
 */

public class ClassroomListActivity extends SingleFragmentActivity {

    public static final String EXTRA_CLASSROOM_ID = "pl.archgriffin.vr.classroom_id";

    @Override
    protected Fragment createFragment() {
        return ClassroomListFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ClassroomListActivity.class);
        return intent;
    }

    public static long getClassroomId(Intent intent) {
        return intent.getLongExtra(EXTRA_CLASSROOM_ID, 0);
    }

}

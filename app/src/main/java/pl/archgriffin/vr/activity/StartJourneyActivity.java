package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pl.archgriffin.vr.fragment.StartJourneyFragment;

/**
 * Created by tomas on 01.04.2017.
 */

public class StartJourneyActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return StartJourneyFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, StartJourneyActivity.class);
        return intent;
    }
}

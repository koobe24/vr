package pl.archgriffin.vr.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import pl.archgriffin.vr.fragment.ReportBugFragment;

/**
 * Created by tomas on 01.04.2017.
 */

public class ReportBugActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return ReportBugFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ReportBugActivity.class);
        return intent;
    }

}

package pl.archgriffin.vr.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.datasource.EmployeeLab;

/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeDetailFragment extends Fragment {
    private static final String TAG = EmployeeDetailFragment.class.getSimpleName();

    private static final String ARG_EMPLOYEE_ID = "EMPLOYEE_ID";
    private static final String DIALOG_PHOTO = "DialogPhoto";

    private ImageView mPhoto;
    private ImageView mTimetablePhoto;
    private TextView mTitleName;
    private TextView mSurname;
    private TextView mWebsite;

    private Employee mEmployee;

    public static EmployeeDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_EMPLOYEE_ID, id);
        EmployeeDetailFragment fragment = new EmployeeDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long employeeId = getArguments().getLong(ARG_EMPLOYEE_ID);
        mEmployee = EmployeeLab.getInstance(getActivity()).getEmployee(employeeId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee, container, false);
        mPhoto = (ImageView) view.findViewById(R.id.employee_photo);
        if (mEmployee.getPhotoPath() != null && !mEmployee.getPhotoPath().isEmpty()) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getAssets().open("employee_photo/" + mEmployee.getPhotoPath()));
                mPhoto.setImageBitmap(bitmap);
                mPhoto.setBackground(null);
            } catch (IOException e) {
                Log.e(TAG, "onCreateView: ", e);
            }
        }
        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEmployee.getPhotoPath() != null && !mEmployee.getPhotoPath().isEmpty()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    PhotoFragment dialog = PhotoFragment.newInstance("employee_photo/" + mEmployee.getPhotoPath());
                    dialog.show(fragmentManager, DIALOG_PHOTO);
                }
            }
        });
        mTimetablePhoto = (ImageView) view.findViewById(R.id.employee_timetable);
        if (mEmployee.getTimetablePath() != null && !mEmployee.getTimetablePath().isEmpty()) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getAssets().open("employee_timetable/" + mEmployee.getTimetablePath()));
                mTimetablePhoto.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.e(TAG, "onCreateView: ", e);
            }
        }
        mTimetablePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEmployee.getTimetablePath() != null && !mEmployee.getTimetablePath().isEmpty()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    PhotoFragment dialog = PhotoFragment.newInstance("employee_timetable/" + mEmployee.getTimetablePath());
                    dialog.show(fragmentManager, DIALOG_PHOTO);
                }
            }
        });
        mTitleName = (TextView) view.findViewById(R.id.employee_title_name);
        mTitleName.setText(mEmployee.getTitle() + " " + mEmployee.getFirstName());
        mSurname = (TextView) view.findViewById(R.id.employee_title_surname);
        mSurname.setText(mEmployee.getSurname());
        mWebsite = (TextView) view.findViewById(R.id.employee_website);
        mWebsite.setText(mEmployee.getWebsite());
        return view;
    }
}

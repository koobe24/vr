package pl.archgriffin.vr.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.activity.ClassroomListActivity;
import pl.archgriffin.vr.activity.ClassroomPagerActivity;
import pl.archgriffin.vr.activity.PanoramaActivity;
import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.datasource.ClassroomLab;

/**
 * Created by tomas on 12.03.2017.
 */

public class ClassroomListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ClassroomAdapter mAdapter;

    public static ClassroomListFragment newInstance() {
        ClassroomListFragment fragment = new ClassroomListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classroom_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.classroom_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ClassroomLab classroomLab = ClassroomLab.getInstance(getActivity());
        mAdapter = new ClassroomAdapter(classroomLab.getClassrooms());
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    private class ClassroomHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mNumber;
        private TextView mInfo;
        private ImageButton mNavigateTo;
        private Classroom mClassroom;

        public ClassroomHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mNumber = (TextView) itemView.findViewById(R.id.list_item_classroom_number);
            mNavigateTo = (ImageButton) itemView.findViewById(R.id.list_item_navigate_to_button);
            if (getActivity().getCallingActivity() != null) {
                mNavigateTo.setVisibility(View.GONE);
            }
            mInfo = (TextView) itemView.findViewById(R.id.list_item_classroom_info);
        }

        public void bindClassroom(Classroom classroom) {
            mClassroom = classroom;
            mNumber.setText(classroom.getNumber());
            mInfo.setText(classroom.getInfo());
            mNavigateTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = PanoramaActivity.newIntent(getActivity(), null, mClassroom);
                    startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View view) {
            if (getActivity().getCallingActivity() == null) {
                Intent intent = ClassroomPagerActivity.newIntent(getActivity(), mClassroom.getId());
                startActivity(intent);
            } else {
                Intent data = new Intent();
                data.putExtra(ClassroomListActivity.EXTRA_CLASSROOM_ID, mClassroom.getId());
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
            }
        }
    }

    private class ClassroomAdapter extends RecyclerView.Adapter<ClassroomHolder> {
        private List<Classroom> mClassrooms;

        public ClassroomAdapter(List<Classroom> classrooms) {
            mClassrooms = classrooms;
        }

        @Override
        public ClassroomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_classroom, parent, false);
            return new ClassroomHolder(view);
        }

        @Override
        public void onBindViewHolder(ClassroomHolder holder, int position) {
            Classroom classroom = mClassrooms.get(position);
            holder.bindClassroom(classroom);
        }

        @Override
        public int getItemCount() {
            return mClassrooms.size();
        }
    }
}

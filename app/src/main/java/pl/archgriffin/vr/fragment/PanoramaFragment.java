package pl.archgriffin.vr.fragment;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.j256.ormlite.dao.Dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.archgriffin.vr.R;
import pl.archgriffin.vr.bean.Photo;
import pl.archgriffin.vr.bean.Transition;
import pl.archgriffin.vr.util.DatabaseHelper;

/**
 * Created by tomas on 01.04.2017.
 */

public class PanoramaFragment extends Fragment {
    private static final String TAG = PanoramaFragment.class.getSimpleName();
    private static final String START_ID = "start_id";
    private static final String DESTINATION_ID = "destination_id";
    private static final float ROTATION_LIMIT = 30;

    public boolean mLoadImageSuccessful;
    @BindView(R.id.panorama_view)
    VrPanoramaView mPanoramaView;
    @BindView(R.id.move_button)
    Button mMoveButton;
    @BindView(R.id.previous_button)
    Button mPreviousButton;
    private VrPanoramaView.Options mPanoramaOptions = new VrPanoramaView.Options();
    private ImageLoaderTask mImageLoaderTask;

    private Photo mCurrentPhoto;
    private Deque<Photo> mPreviousPhotos;
    private Queue<Photo> mToDestinationPhotos;
    private long mShortestQueue = Long.MAX_VALUE;
    private long destinationId;
    private Unbinder mUnbinder;
    private Dao<Photo, Long> photoDao;

    public static PanoramaFragment newInstance(long start, long destination) {
        Log.i(TAG, "newInstance: " + start + " " + destination);
        Bundle args = new Bundle();
        args.putLong(START_ID, start);
        args.putLong(DESTINATION_ID, destination);
        PanoramaFragment fragment = new PanoramaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreviousPhotos = new ArrayDeque<>();
        mToDestinationPhotos = new LinkedList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            photoDao = (new DatabaseHelper(getActivity())).getPhotoDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        View view = inflater.inflate(R.layout.fragment_panorama, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mPanoramaView.setEventListener(new PanoramaEventListener());
        mMoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveForward();
            }
        });
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToPrevious();
            }
        });
        Bundle args = getArguments();
        long startId = args.getLong(START_ID, 1);
        Log.i(TAG, "onCreateView: " + startId);
        destinationId = args.getLong(DESTINATION_ID, -1);
        loadImageById(startId);
        return view;
    }

    private void loadImageById(long photoId) {
        try {
            Log.i(TAG, "loadImageById: photoid " + photoId);
            mCurrentPhoto = photoDao.queryForId(photoId);
            buildToDestinationPathQueue();
        } catch (SQLException e) {
            Log.e(TAG, "Error while trying to get Photo DAO");
            e.printStackTrace();
        }
        loadImage();
    }

    private void loadImage() {
        Uri fileUri = Uri.parse(mCurrentPhoto.getPath());
        mPanoramaOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
        if (mImageLoaderTask != null) {
            mImageLoaderTask.cancel(true);
        }
        mImageLoaderTask = new PanoramaFragment.ImageLoaderTask();
        mImageLoaderTask.execute(Pair.create(fileUri, mPanoramaOptions));
    }

    @Override
    public void onDestroyView() {
        mPanoramaView.shutdown();
        mUnbinder.unbind();
        if (mImageLoaderTask != null) {
            mImageLoaderTask.cancel(true);
        }
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        mPanoramaView.pauseRendering();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPanoramaView.resumeRendering();
    }

    public void moveForward() {
        Log.i(TAG, "moveForward: ");
        makeMove();
    }

    public void moveToPrevious() {
        if (!mPreviousPhotos.isEmpty()) {
            mCurrentPhoto = mPreviousPhotos.pop();
            try {
                buildToDestinationPathQueue();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            loadImage();
        }
    }

    public void makeMove() {
        Transition closest = getCurrentMinTransition();
        if (closest != null) {
            if (!removeIfLastPrevious(closest.getDestination().getId())) {
                mPreviousPhotos.push(mCurrentPhoto);
            }
            loadImageById(closest.getDestination().getId());
        }
    }

    private Transition getCurrentMinTransition() {
        Transition closest = null;
        float minDistance = ROTATION_LIMIT;
        for (Transition t : mCurrentPhoto.getTransitions()) {
            float currentDistance = getDistanceBetweenRotations(t.getRotationNeeded());
            if (minDistance > currentDistance) {
                minDistance = currentDistance;
                closest = t;
            }
        }
        return closest;
    }

    private float getDistanceBetweenRotations(float rotationNeeded) {
        float currentDistance;
        float currentHorizontal = getCurrentHorizontalRotation();
        if (currentHorizontal * rotationNeeded >= 0) {
            currentDistance = Math.abs(rotationNeeded - currentHorizontal);
        } else {
            currentDistance = Math.abs(rotationNeeded) + Math.abs(currentHorizontal);
            if (currentDistance > 180) {
                currentDistance = 360 - currentDistance;
            }
        }
        return currentDistance;
    }

    private float getCurrentHorizontalRotation() {
        float[] currentRotation = new float[2];
        mPanoramaView.getHeadRotation(currentRotation);
        return currentRotation[0];
    }

    private boolean removeIfLastPrevious(long id) {
        if (mPreviousPhotos.peek() != null && mPreviousPhotos.peek().getId() == id) {
            mPreviousPhotos.pop();
            return true;
        }
        return false;
    }

    private Transition getTransitionToNextFromPath() {
        Photo next = mToDestinationPhotos.peek();
        for (Transition transition : mCurrentPhoto.getTransitions()) {
            try {
                if (photoDao.queryForId(transition.getDestination().getId()).equals(next))
                    return transition;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean checkIfMovedWithPath() {
        if (mCurrentPhoto.equals(mToDestinationPhotos.peek())) return true;
        else return false;
    }

    private boolean buildShortestPathToDestination() throws SQLException {
        int shortestLength = Integer.MAX_VALUE;
        List<Queue<Photo>> queuesList = buildAllPathsToDestination();
        if (!queuesList.isEmpty()) {
            for (Queue<Photo> queue : queuesList) {
                if (queue.size() < shortestLength) {
                    shortestLength = queue.size();
                    mToDestinationPhotos = queue;
                }
            }
            return true;
        } else return false;
    }

    private List<Queue<Photo>> buildAllPathsToDestination() throws SQLException {
        List<Queue<Photo>> queuesList = new ArrayList<>();
        for (Transition transition : mCurrentPhoto.getTransitions()) {
            Queue<Photo> queue = new ArrayDeque<>();
            queue.offer(photoDao.queryForId(transition.getDestination().getId()));
            if (transition.getDestination().getId() != destinationId) {
                buildPathFromPhoto(queuesList, queue, photoDao.queryForId(transition.getDestination().getId()), mCurrentPhoto);
            } else {
                mShortestQueue = queue.size();
                queuesList.add(queue);
            }
        }
        return queuesList;
    }

    private void buildPathFromPhoto(List<Queue<Photo>> queuesList, Queue<Photo> actualQueue, Photo actualPhoto, Photo previous) throws SQLException {
        if (actualQueue.size() < mShortestQueue) {
            for (Transition transition : actualPhoto.getTransitions()) {
                if (!photoDao.queryForId(transition.getDestination().getId()).equals(previous)) {
                    if (!actualQueue.contains(photoDao.queryForId(transition.getDestination().getId()))) {
                        Queue<Photo> newQueue = new ArrayDeque<>(actualQueue);
                        newQueue.offer(photoDao.queryForId(transition.getDestination().getId()));
                        if (transition.getDestination().getId() != destinationId) {
                            buildPathFromPhoto(queuesList, newQueue, photoDao.queryForId(transition.getDestination().getId()), actualPhoto);
                        } else {
                            mShortestQueue = newQueue.size();
                            queuesList.add(newQueue);
                        }
                    }
                }
            }
        }
        actualQueue.clear();
    }

    private void buildToDestinationPathQueue() throws SQLException {
        if (destinationId != -1) {
            if (checkIfMovedWithPath()) {
                mToDestinationPhotos.poll();
            } else {
                mShortestQueue = Long.MAX_VALUE;
                mToDestinationPhotos = null;
                buildShortestPathToDestination();
            }
        }
    }

    class ImageLoaderTask extends AsyncTask<Pair<Uri, VrPanoramaView.Options>, Void, Boolean> {
        VrPanoramaView.Options panoramaOptions = null;
        InputStream inputStream = null;

        @Override
        protected Boolean doInBackground(Pair<Uri, VrPanoramaView.Options>... fileInformation) {
            AssetManager assetManager = getActivity().getAssets();
            try {
                inputStream = assetManager.open(fileInformation[0].first.toString());
                panoramaOptions = new VrPanoramaView.Options();
                panoramaOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
            } catch (IOException e) {
                Log.e(TAG, "Could not load file: " + e);
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            bitmapWithArrows(bitmap);
            if (destinationId != -1 && destinationId != mCurrentPhoto.getId()) {
                Transition transition = getTransitionToNextFromPath();
                if (transition != null) {
                    bitmapWithDestinationArrow(bitmap, transition);
                }
            }
            mPanoramaView.loadImageFromBitmap(bitmap, panoramaOptions);
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close input stream " + e);
            }
            super.onPostExecute(aBoolean);
        }

        private void bitmapWithArrows(Bitmap bitmap) {
            Bitmap arrow = BitmapFactory.decodeResource(getResources(), R.drawable.arrow_up);
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
            Log.i(TAG, "bitmapWithArrows: " + mCurrentPhoto.getTransitions());
            for (Transition t : mCurrentPhoto.getTransitions()) {
                float rotation = t.getRotationNeeded();
                int x = (int) (rotation / 360.0 * canvas.getWidth() + canvas.getWidth() / 2) - arrow.getWidth() / 2;
                int y = (int) (canvas.getHeight() / 2.0) - arrow.getHeight() / 2;
                canvas.drawBitmap(arrow, x, y, paint);
                if (rotation == 180) {
                    rotation = -179;
                    x = (int) (rotation / 360.0 * canvas.getWidth() + canvas.getWidth() / 2) - arrow.getWidth() / 2;
                    y = (int) (canvas.getHeight() / 2.0) - arrow.getHeight() / 2;
                    canvas.drawBitmap(arrow, x, y, paint);
                }
                Log.i(TAG, "bitmapWithArrows: " + x + " " + y);
                Log.i(TAG, "bitmapWithArrows: " + rotation);
            }
        }

        private void bitmapWithDestinationArrow(Bitmap bitmap, Transition next) {
            Bitmap arrow = BitmapFactory.decodeResource(getResources(), R.drawable.green_arrow);
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
            Log.i(TAG, "bitmapWithArrow: " + next);
            float rotation = next.getRotationNeeded();
            int x = (int) (rotation / 360.0 * canvas.getWidth() + canvas.getWidth() / 2) - arrow.getWidth() / 2;
            int y = (int) (canvas.getHeight() / 2.0) - arrow.getHeight() / 2;
            canvas.drawBitmap(arrow, x, y, paint);
            if (rotation == 180) {
                rotation = -179;
                x = (int) (rotation / 360.0 * canvas.getWidth() + canvas.getWidth() / 2) - arrow.getWidth() / 2;
                y = (int) (canvas.getHeight() / 2.0) - arrow.getHeight() / 2;
                canvas.drawBitmap(arrow, x, y, paint);
            }
            Log.i(TAG, "bitmapWithArrow: " + x + " " + y);
            Log.i(TAG, "bitmapWithArrow: " + rotation);
        }

    }

    private class PanoramaEventListener extends VrPanoramaEventListener {

        @Override
        public void onClick() {
            makeMove();
        }

        @Override
        public void onLoadSuccess() {
            mLoadImageSuccessful = true;
        }

        @Override
        public void onLoadError(String errorMessage) {
            mLoadImageSuccessful = false;
            Log.e(TAG, "Error loading pano: " + errorMessage);
            super.onLoadError(errorMessage);
        }
    }


}
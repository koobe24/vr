package pl.archgriffin.vr.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.activity.PanoramaActivity;
import pl.archgriffin.vr.adapter.EmployeeAdapter;
import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.datasource.ClassroomLab;


/**
 * Created by tomas on 12.03.2017.
 */

public class ClassroomDetailFragment extends Fragment {
    private static final String TAG = ClassroomDetailFragment.class.getSimpleName();
    private static final String ARG_CLASSROOM_ID = "classroom_id";

    private static final String DIALOG_PHOTO = "DialogPhoto";

    private ImageView mTimetablePhoto;
    private TextView mNumber;
    private TextView mInfo;
    private ImageButton mNavigateTo;
    private RecyclerView mEmployeeList;

    private Classroom mClassroom;

    public static ClassroomDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_CLASSROOM_ID, id);
        ClassroomDetailFragment fragment = new ClassroomDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long classroomId = getArguments().getLong(ARG_CLASSROOM_ID);
        mClassroom = ClassroomLab.getInstance(getActivity()).getClassroom(classroomId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classroom, container, false);
        mTimetablePhoto = (ImageView) view.findViewById(R.id.classroom_timetable);
        if (mClassroom.getTimetable() != null && !mClassroom.getTimetable().isEmpty()) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getAssets().open("classroom_timetable/" + mClassroom.getTimetable()));
                mTimetablePhoto.setImageBitmap(bitmap);
                mTimetablePhoto.setBackground(null);
            } catch (IOException e) {
                Log.e(TAG, "onCreateView: ", e);
            }
        }
        mTimetablePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClassroom.getTimetable() != null && !mClassroom.getTimetable().isEmpty()) {
                    FragmentManager fragmentManager = getFragmentManager();
                    PhotoFragment dialog = PhotoFragment.newInstance("classroom_timetable/" + mClassroom.getTimetable());
                    dialog.show(fragmentManager, DIALOG_PHOTO);
                }
            }
        });
        mNumber = (TextView) view.findViewById(R.id.classroom_number);
        mNumber.setText(mClassroom.getNumber());
        mInfo = (TextView) view.findViewById(R.id.classroom_info);
        mInfo.setText(mClassroom.getInfo());
        mNavigateTo = (ImageButton) view.findViewById(R.id.navigate_to_button);
        mNavigateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNavigateTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = PanoramaActivity.newIntent(getActivity(), null, mClassroom);
                        startActivity(intent);
                    }
                });
            }
        });
        if (mClassroom.getEmployees() != null && !mClassroom.getEmployees().isEmpty()) {
            List<Employee> employees = mClassroom.getEmployees();
            mEmployeeList = (RecyclerView) view.findViewById(R.id.classroom_fragment_employees_recycle_view);
            mEmployeeList.setLayoutManager(new LinearLayoutManager(getActivity()));
            EmployeeAdapter adapter = new EmployeeAdapter(getActivity(), employees);
            mEmployeeList.setAdapter(adapter);
        }
        return view;
    }
}

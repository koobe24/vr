package pl.archgriffin.vr.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.adapter.EmployeeAdapter;
import pl.archgriffin.vr.datasource.EmployeeLab;


/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private EmployeeAdapter mAdapter;

    public static EmployeeListFragment newInstance() {
        EmployeeListFragment fragment = new EmployeeListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.employee_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        EmployeeLab employeeLab = EmployeeLab.getInstance(getActivity());
        mAdapter = new EmployeeAdapter(getActivity(),employeeLab.getEmployees());
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

}

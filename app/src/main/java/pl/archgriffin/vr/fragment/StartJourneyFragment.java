package pl.archgriffin.vr.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.archgriffin.vr.activity.PanoramaActivity;
import pl.archgriffin.vr.R;
import pl.archgriffin.vr.activity.ClassroomListActivity;
import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.util.DatabaseHelper;

/**
 * Created by tomas on 01.04.2017.
 */

public class StartJourneyFragment extends Fragment {

    private static final int REQUEST_START_CODE = 0;
    private static final int REQUEST_DESTINATION_CODE = 1;

    @BindView(R.id.select_start_button)
    Button mSelectStart;
    @BindView(R.id.clear_start_button)
    Button mClearStart;
    @BindView(R.id.select_destination_button)
    Button mSelectDestination;
    @BindView(R.id.clear_destination_button)
    Button mClearDestination;
    @BindView(R.id.journey_start_number)
    TextView mJourneyStartTextView;
    @BindView(R.id.journey_destination_number)
    TextView mJourneyDestinationTextView;
    @BindView(R.id.start_journey_button)
    Button mStartJourney;

    private Unbinder mUnbinder;

    private Classroom mStartClassroom;
    private Classroom mDestinationClassroom;
    private Dao<Classroom, Long> mClassroomDao;

    public static StartJourneyFragment newInstance() {
        StartJourneyFragment fragment = new StartJourneyFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_journey, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        mStartJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = PanoramaActivity.newIntent(getActivity(), mStartClassroom, mDestinationClassroom);
                startActivity(i);
            }
        });
        mSelectStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = ClassroomListActivity.newIntent(getActivity());
                startActivityForResult(i, REQUEST_START_CODE);
            }
        });
        mSelectDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = ClassroomListActivity.newIntent(getActivity());
                startActivityForResult(i, REQUEST_DESTINATION_CODE);
            }
        });
        mClearDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearDestinationClassroom();
            }
        });
        mClearStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearStartClassroom();
            }
        });
        try {
            mClassroomDao = new DatabaseHelper(getActivity()).getClassroomDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            long id = ClassroomListActivity.getClassroomId(data);
            if (requestCode == REQUEST_START_CODE) {
                try {
                    mStartClassroom = mClassroomDao.queryForId(id);
                    mJourneyStartTextView.setText(mStartClassroom.getNumber());
                } catch (SQLException e) {
                    e.printStackTrace();
                    clearStartClassroom();
                }
            } else if (requestCode == REQUEST_DESTINATION_CODE) {
                try {
                    mDestinationClassroom = mClassroomDao.queryForId(id);
                    mJourneyDestinationTextView.setText(mDestinationClassroom.getNumber());
                } catch (SQLException e) {
                    e.printStackTrace();
                    clearDestinationClassroom();
                }
            }
        }
    }

    private void clearStartClassroom() {
        mJourneyStartTextView.setText("");
        mStartClassroom = null;
    }

    private void clearDestinationClassroom() {
        mJourneyDestinationTextView.setText("");
        mDestinationClassroom = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}

package pl.archgriffin.vr.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.IOException;

import pl.archgriffin.vr.R;


/**
 * Created by tomas on 12.03.2017.
 */
public class PhotoFragment extends DialogFragment {
    private static final String TAG = PhotoFragment.class.getSimpleName();
    private static final String ARG_PHOTO_FILE = "photo_path";

    private String mPhotoPath;
    private ImageView mPhotoView;

    public static PhotoFragment newInstance(String photoPath) {
        Bundle args = new Bundle();
        args.putString(ARG_PHOTO_FILE, photoPath);
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPhotoPath = getArguments().getString(ARG_PHOTO_FILE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo, container, false);
        mPhotoView = (ImageView) v.findViewById(R.id.photo_fragment_view);
        if (mPhotoPath != null && !mPhotoPath.isEmpty()) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(getActivity().getAssets().open(mPhotoPath));
                mPhotoView.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.e(TAG, "onCreateView: ", e);
            }
        }
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return v;
    }
}

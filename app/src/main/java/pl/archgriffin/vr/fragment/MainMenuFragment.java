package pl.archgriffin.vr.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.archgriffin.vr.R;
import pl.archgriffin.vr.activity.ClassroomListActivity;
import pl.archgriffin.vr.activity.EmployeeListActivity;
import pl.archgriffin.vr.activity.ReportBugActivity;
import pl.archgriffin.vr.activity.StartJourneyActivity;

/**
 * Created by tomas on 01.04.2017.
 */

public class MainMenuFragment extends Fragment {

    @BindView(R.id.report_bug_button)
    Button mBugButton;
    @BindView(R.id.start_journey_button)
    Button mStartJourneyButton;
    @BindView(R.id.check_classrooms_button)
    Button mCheckClassrooms;
    @BindView(R.id.check_employees_button)
    Button mCheckEmployees;

    private Unbinder mUnbinder;

    public static MainMenuFragment newInstance() {
        MainMenuFragment fragment = new MainMenuFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        mBugButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = ReportBugActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });

        mStartJourneyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = StartJourneyActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });
        mCheckClassrooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = ClassroomListActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });
        mCheckEmployees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = EmployeeListActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}

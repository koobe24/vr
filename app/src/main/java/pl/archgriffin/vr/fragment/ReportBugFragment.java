package pl.archgriffin.vr.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.archgriffin.vr.R;

/**
 * Created by tomas on 01.04.2017.
 */

public class ReportBugFragment extends Fragment {

    @BindView(R.id.topic_spinner)
    Spinner mTopicSpinner;
    @BindView(R.id.report)
    EditText mReport;
    @BindView(R.id.reporter_name)
    EditText mReporterName;

    private Unbinder mUnbinder;

    public static ReportBugFragment newInstance() {
        return new ReportBugFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_bug, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.report_topics, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTopicSpinner.setAdapter(adapter);
        return view;
    }

    @OnClick(R.id.send_report)
    public void sendReport() {
        boolean canSend = true;
        if (mReport.getText().toString().isEmpty()) {
            canSend = false;
            mReport.setError(getText(R.string.edit_text_not_empty));
        }
        if (mReporterName.getText().toString().isEmpty()) {
            canSend = false;
            mReporterName.setError(getText(R.string.edit_text_not_empty));
        }
        if (canSend) {
            // TODO: 01.04.2017 wyslij
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("message/rfc822");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"przykladowymail@gmail.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "VR app - " + ((TextView) mTopicSpinner.getSelectedView()).getText());
            emailIntent.putExtra(Intent.EXTRA_TEXT, mReport.getText() + "\n" + mReporterName.getText());
            startActivity(Intent.createChooser(emailIntent, getText(R.string.send_mail)));
            getActivity().finish();
        }
    }

    @OnClick(R.id.clear_report)
    public void clearReport() {
        mReporterName.setText("");
        mReport.setText("");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}

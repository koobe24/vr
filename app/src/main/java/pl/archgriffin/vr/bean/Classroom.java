package pl.archgriffin.vr.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by tomas on 12.03.2017.
 */
@DatabaseTable(tableName = Classroom.TABLE_NAME_CLASSROOM)
public class Classroom {
    public static final String TABLE_NAME_CLASSROOM = "classrooms";
    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_NUMBER = "number";
    public static final String FIELD_NAME_TIMETABLE = "timetable";
    public static final String FIELD_NAME_INFO = "info";
    public static final String FIELD_NAME_PHOTO_ID = "photoId";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private long mId;
    @DatabaseField(columnName = FIELD_NAME_NUMBER)
    private String mNumber;
    @DatabaseField(columnName = FIELD_NAME_TIMETABLE)
    private String mTimetable;
    @DatabaseField(columnName = FIELD_NAME_INFO)
    private String mInfo;
    @DatabaseField(columnName = FIELD_NAME_PHOTO_ID)
    private long mPhotoId;
    private List<Employee> mEmployees;

    public Classroom() {
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getTimetable() {
        return mTimetable;
    }

    public void setTimetable(String timetable) {
        mTimetable = timetable;
    }

    public long getmPhotoId() {
        return mPhotoId;
    }

    public void setmPhotoId(long mPhotoId) {
        this.mPhotoId = mPhotoId;
    }

    public List<Employee> getEmployees() {
        return mEmployees;
    }

    public void setEmployees(List<Employee> employees) {
        mEmployees = employees;
    }
}

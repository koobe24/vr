package pl.archgriffin.vr.bean;

import android.support.annotation.NonNull;
import android.util.Log;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tomas on 12.03.2017.
 */

@DatabaseTable(tableName = Employee.TABLE_NAME)

public class Employee implements Comparable<Employee> {
    private static final String TAG = Employee.class.getSimpleName();
    public static final String TABLE_NAME = "employees";
    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_TITLE = "title";
    public static final String FIELD_NAME_FIRST = "first";
    public static final String FIELD_NAME_SUR = "sur";
    public static final String FIELD_NAME_TIMETABLE = "timetable";
    public static final String FIELD_NAME_PHOTO = "photo";
    public static final String FIELD_NAME_WEBSITE = "website";
    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private long mId;
    @DatabaseField(columnName = FIELD_NAME_TITLE)
    private String mTitle="";
    @DatabaseField(columnName = FIELD_NAME_FIRST)
    private String mFirstName;
    @DatabaseField(columnName = FIELD_NAME_SUR)
    private String mSurname;
    @DatabaseField(columnName = FIELD_NAME_TIMETABLE)
    private String mTimetablePath;
    @DatabaseField(columnName = FIELD_NAME_PHOTO)
    private String mPhotoPath="";
    @DatabaseField(columnName = FIELD_NAME_WEBSITE)
    private String mWebsite;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getTimetablePath() {
        return mTimetablePath;
    }

    public void setTimetablePath(String timetablePath) {
        mTimetablePath = timetablePath;
    }

    public String getPhotoPath() {
        return mPhotoPath;
    }

    public void setPhotoPath(String photoPath) {
        mPhotoPath = photoPath;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String website) {
        mWebsite = website;
    }

    @Override
    public int compareTo(@NonNull Employee employee) {
        Log.i(TAG, "compareTo: " + (employee == null) + " " + (employee.getSurname() == null));
        int compare = this.getSurname().compareTo(employee.getSurname());
        if (compare == 0) {
            compare = this.getFirstName().compareTo(employee.getFirstName());
        }
        return compare;
    }
}

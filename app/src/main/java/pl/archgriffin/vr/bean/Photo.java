package pl.archgriffin.vr.bean;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tomas on 13.12.2016.
 */

@DatabaseTable(tableName = Photo.TABLE_NAME_PHOTOS)
public class Photo {

    public static final String TABLE_NAME_PHOTOS = "photos";
    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_X = "x";
    public static final String FIELD_NAME_Y = "y";
    public static final String FIELD_NAME_PATH = "path";
    public static final String FIELD_NAME_DESCRIPTION = "description";
    public static final String FIELD_NAME_TRANSITIONS = "transitions";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private long id;
    @DatabaseField(columnName = FIELD_NAME_X)
    private int x;
    @DatabaseField(columnName = FIELD_NAME_Y)
    private int y;
    @DatabaseField(columnName = FIELD_NAME_PATH)
    private String path;
    @DatabaseField(columnName = FIELD_NAME_DESCRIPTION)
    private String description;

    @ForeignCollectionField(columnName = FIELD_NAME_TRANSITIONS, foreignFieldName = Transition.FIELD_NAME_FROM)
    private ForeignCollection<Transition> transitions;

    public Photo(int x, int y, String path) {
        this.x = x;
        this.y = y;
        this.path = path;
    }

    public Photo(int x, int y, String path, String description) {
        this.x = x;
        this.y = y;
        this.path = path;
        this.description = description;
    }

    public Photo() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ForeignCollection<Transition> getTransitions() {
        return transitions;
    }

    public void setTransitions(ForeignCollection<Transition> transitions) {
        this.transitions = transitions;
    }

    @Override
    public boolean equals(Object obj) {
        boolean areEqual = false;
        if (obj!=null && obj instanceof Photo) {
            if (((Photo) obj).getX() == this.getX() && ((Photo) obj).getY() == this.getY()) {
                areEqual = true;
            }
        }
        return areEqual;
    }
}

package pl.archgriffin.vr.bean;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import static pl.archgriffin.vr.bean.Transition.TABLE_NAME_TRANSITIONS;

/**
 * Created by tomas on 13.12.2016.
 */

@DatabaseTable(tableName = TABLE_NAME_TRANSITIONS)
public class Transition {

    public static final String TABLE_NAME_TRANSITIONS = "transitions";
    public static final String FIELD_NAME_ID = "id";
    public static final String FIELD_NAME_FROM = "from";
    public static final String FIELD_NAME_DESTINATION = "destination";
    public static final String FIELD_NAME_ROTATION_NEEDED = "rotation_needed";

    @DatabaseField(columnName = FIELD_NAME_ID, generatedId = true)
    private long id;
    @DatabaseField(columnName = FIELD_NAME_FROM, foreign = true)
    private Photo from;
    @DatabaseField(columnName = FIELD_NAME_DESTINATION, foreign = true)
    private Photo destination;
    @DatabaseField(columnName = FIELD_NAME_ROTATION_NEEDED)
    private float rotationNeeded;

    public Transition(Photo from, Photo destination, float rotationNeeded) {
        this.from = from;
        this.destination = destination;
        this.rotationNeeded = rotationNeeded;
    }

    public Transition(Photo from, Photo destination) {
        this.from = from;
        this.destination = destination;
    }

    public Transition() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Photo getDestination() {
        return destination;
    }

    public void setDestination(Photo destination) {
        this.destination = destination;
    }

    public float getRotationNeeded() {
        return rotationNeeded;
    }

    public void setRotationNeeded(float rotationNeeded) {
        this.rotationNeeded = rotationNeeded;
    }
}

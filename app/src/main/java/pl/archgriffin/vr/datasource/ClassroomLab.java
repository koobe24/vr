package pl.archgriffin.vr.datasource;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.archgriffin.vr.bean.Classroom;
import pl.archgriffin.vr.util.DatabaseHelper;


/**
 * Created by tomas on 12.03.2017.
 */

public class ClassroomLab {
    private static ClassroomLab sClassroomLab;

    private List<Classroom> mClassrooms;

    public static ClassroomLab getInstance(Context context) {
        if (sClassroomLab == null) {
            sClassroomLab = new ClassroomLab(context);
        }
        return sClassroomLab;
    }

    private ClassroomLab(Context context) {
        mClassrooms = new ArrayList<>();
        try {
            Dao<Classroom, Long> classroomDao = new DatabaseHelper(context).getClassroomDao();
            mClassrooms = classroomDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Classroom> getClassrooms() {
        return mClassrooms;
    }

    public Classroom getClassroom(long classroomId) {
        for (Classroom c : mClassrooms) {
            if (c.getId() == classroomId) {
                return c;
            }
        }
        return null;
    }
}

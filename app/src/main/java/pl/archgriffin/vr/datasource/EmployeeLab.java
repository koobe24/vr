package pl.archgriffin.vr.datasource;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.archgriffin.vr.bean.Employee;
import pl.archgriffin.vr.util.DatabaseHelper;


/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeLab {

    private static EmployeeLab sEmployeeLab;

    private List<Employee> mEmployees;

    public static EmployeeLab getInstance(Context context) {
        if (sEmployeeLab == null) {
            sEmployeeLab = new EmployeeLab(context);
        }
        return sEmployeeLab;
    }

    private EmployeeLab(Context context) {
        mEmployees = new ArrayList<>();
        try {
            Dao<Employee, Long> employeeDao = new DatabaseHelper(context).getEmployeeDao();
            mEmployees = employeeDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collections.sort(mEmployees);
    }

    public List<Employee> getEmployees() {
        return mEmployees;
    }

    public Employee getEmployee(long id) {
        for (Employee e : mEmployees) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }
}

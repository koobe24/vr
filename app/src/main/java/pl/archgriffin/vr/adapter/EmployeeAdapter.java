package pl.archgriffin.vr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.holder.EmployeeHolder;
import pl.archgriffin.vr.bean.Employee;


/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeHolder> {
    private List<Employee> mEmployees;
    private Context mContext;

    public EmployeeAdapter(Context context, List<Employee> employees) {
        mContext = context;
        mEmployees = employees;
    }

    @Override
    public EmployeeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.list_item_employee, parent, false);
        return new EmployeeHolder(mContext, view);
    }

    @Override
    public void onBindViewHolder(EmployeeHolder holder, int position) {
        Employee employee = mEmployees.get(position);
        holder.bindEmployee(employee);
    }

    @Override
    public int getItemCount() {
        return mEmployees.size();
    }
}
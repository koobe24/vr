package pl.archgriffin.vr.holder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import pl.archgriffin.vr.R;
import pl.archgriffin.vr.activity.EmployeePagerActivity;
import pl.archgriffin.vr.bean.Employee;


/**
 * Created by tomas on 12.03.2017.
 */

public class EmployeeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TAG = EmployeeHolder.class.getSimpleName();
    private ImageView mPhoto;
    private TextView mTitleName;
    private TextView mSurname;
    private Employee mEmployee;
    private Context mContext;

    public EmployeeHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        itemView.setOnClickListener(this);
        mPhoto = (ImageView) itemView.findViewById(R.id.list_item_employee_photo);
        mTitleName = (TextView) itemView.findViewById(R.id.list_item_employee_title_name);
        mSurname = (TextView) itemView.findViewById(R.id.list_item_employee_title_surname);
    }

    public void bindEmployee(Employee employee) {
        mEmployee = employee;
        if (mEmployee.getPhotoPath() != null && !mEmployee.getPhotoPath().isEmpty()) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(mContext.getAssets().open("employee_photo/" + mEmployee.getPhotoPath()));
                mPhoto.setImageBitmap(bitmap);
                mPhoto.setBackground(null);
            } catch (IOException e) {
                Log.e(TAG, "onCreateView: ", e);
            }
        } else {
            mPhoto.setImageDrawable(null);
            mPhoto.setBackgroundResource(android.R.color.darker_gray);
        }
        String nameAndTitle = employee.getTitle() + " " + employee.getFirstName();
        mTitleName.setText(nameAndTitle);
        mSurname.setText(employee.getSurname());
    }

    @Override
    public void onClick(View view) {
        Intent intent = EmployeePagerActivity.newIntent(mContext, mEmployee.getId());
        mContext.startActivity(intent);
    }
}
